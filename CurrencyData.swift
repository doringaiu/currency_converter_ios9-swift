//
//  CurrencyData.swift
//  testApplication01
//
//  Created by Dorin Gaiu on 3/24/16.
//  Copyright © 2016 Dorin Gaiu. All rights reserved.
//

import Foundation
import Kanna

class CurrencyData: NSObject, NSXMLParserDelegate {
  static let listOfFlags = [UIImage(named: "Moldova.png"), UIImage(named: "United States.png"), UIImage(named: "European Union.png"),
                            UIImage(named: "Russia.png"), UIImage(named: "Romania.png"), UIImage(named: "Ukraine.png"),
                            UIImage(named: "United Kingdom.png"), UIImage(named: "Switzerland.png"), UIImage(named: "Turkey.png")]
  static let listOfCurrencies = ["USD", "EUR", "RUB", "RON",
                                 "UAH", "GBP", "CHF", "TRY"]
  
  var dataCurs = Array<Dictionary<String, AnyObject>>()
  var storedValues = [String:String]()
  private var httpReply:NSData?
  private var xmlParser:NSXMLParser?
  private var lastTag:String?
  private var tagContent:String?
  private var lastCurrency:String?
  
  func parseBNM() {
    var urlStr: String? = "https://www.bnm.md/en/official_exchange_rates?get_xml=1&date="
    urlStr! += getCurrentTime()
    let urlBNM = NSURL(string: urlStr!)
    xmlParser = NSXMLParser(contentsOfURL: urlBNM!)
    xmlParser?.delegate = self
    xmlParser?.parse()
  }
  
  static func isTheSavedDataActual() -> Bool {
    return false
  }
  
  private func getCurrentTime() -> String {
    let requestedComponents: NSCalendarUnit = [NSCalendarUnit.Day, NSCalendarUnit.Month, NSCalendarUnit.Year]
    let dateTimeComponents = NSCalendar.currentCalendar().components(requestedComponents, fromDate: NSDate())
    var dates = [String(format: "%02d", dateTimeComponents.day),
                 String(format: "%02d", dateTimeComponents.month),
                 dateTimeComponents.year.description]
    
    return "\(dates[0]).\(dates[1]).\(dates[2])"
  }
  
  func parser(parser: NSXMLParser, foundCharacters string: String) {
    self.tagContent = string
  }
  
  func parser(parser: NSXMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
    
    switch(lastTag){
      
    case "CharCode"?:
      storedValues[tagContent!] = nil
      lastCurrency = tagContent!
      
    case "Value"?:
      storedValues[lastCurrency!] = tagContent!
      
    default: break
    }
    
    tagContent = nil
    lastTag = nil
  }
  
  func parser(parser: NSXMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String]) {
    lastTag = elementName
  }
  
//  func parserDidEndDocument(parser: NSXMLParser) {
//    
//  }
  
  private func sendHttpRequest() {
    let url = NSURL(string: "http://www.curs.md/en/curs_valutar_banci")
    let session = NSURLSession.sharedSession()
    let request: NSURLRequest = NSURLRequest(URL: url!)
    let semaphore = dispatch_semaphore_create(0)
    session.dataTaskWithRequest(request) { (data, response, error) -> Void in
      // Handle incoming data like you would in synchronous request
      dispatch_semaphore_signal(semaphore)
       self.httpReply = data
    }.resume()
    dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER)
  }
  
  private func parseHTML() {
    if let doc = Kanna.HTML(html: httpReply!, encoding: NSUTF8StringEncoding) {
      
      if let snTable = doc.at_css("table[id='tabelBankValute']") {
        let listOfCurrencies = ["Bank name", "USD-Buy", "USD-Sell", "EUR-Buy", "EUR-Sell", "RUB-Buy", "RUB-Sell", "RON-Buy", "RON-Sell",
                                "UAH-Buy", "UAH-Sell", "GBP-Buy", "GBP-Sell", "CHF-Buy", "CHF-Sell", "TRY-Buy", "TRY-Sell"]
        
        for(_, tr) in snTable.css("tr").enumerate() {
          
          var dictionary = Dictionary<String, AnyObject>()
          
          for(idx, td) in tr.css("td").enumerate() {
            dictionary[listOfCurrencies[idx]] = td.text
            
          }
          dataCurs.append(dictionary)
        }
        
      }
    }
  }
  
  func getData() -> Bool{
    sendHttpRequest()
    NSThread.sleepForTimeInterval(0.1)
    
    if httpReply != nil {
      parseHTML()
      dataCurs.removeRange(0...1)
      let filemgr = ManageFile()
      filemgr.createFile(JSONSerializationClass.serializer(dataCurs))
      return true
    } else {
      return false
    }
  }
  
  static func extractListOfBanks(data: Array<Dictionary<String, AnyObject>>) -> Array<AnyObject> {
    let size = data.count
    var listOfBanks = Array<AnyObject>()
    
    for index in 0...size {
      listOfBanks.append(data[index]["Bank name"]!)
    }
    
    return listOfBanks
  }
  
  static func sortByBestValue(data: Array<Dictionary<String, AnyObject>>, currency: String) -> Array<Dictionary<String, AnyObject>> {
    
    return data.sort { (dict1, dict2) -> Bool in
        let firstNum = Double(dict1[currency] as! String)
        let secNum =  Double(dict2[currency] as! String)
        return firstNum > secNum
    }
  }
  
}




