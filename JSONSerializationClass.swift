//
//  JSONSerializationClass.swift
//  testApplication01
//
//  Created by Dorin Gaiu on 3/25/16.
//  Copyright © 2016 Dorin Gaiu. All rights reserved.
//

import Foundation
import SGYSwiftJSON

class JSONSerializationClass {
  
  static func serializer(data: Array<Dictionary<String, AnyObject>>) -> NSData? {
    let jsonData:NSData?
    let serializer = SGYJSONSerializer()
    
    do {
      jsonData = try serializer.serialize(data)
      return jsonData
      
    } catch let error as NSError {
      print(error)
      return nil
    }
  }
  
  static func deserialize(jsonData: NSData?) -> Array<Dictionary<String, AnyObject>> {
    var dsData = Array<Dictionary<String, AnyObject>>()
    let deserializer = SGYJSONDeserializer()
    
    do {
      dsData = try deserializer.deserialize(jsonData!)
    } catch let error as NSError {
      print(error)
    }
    return dsData
  }
}
