### Currency Converter and Navigator [iOS 9 Swift 2.2]
## Objectives
1. Get currency data from all the banks by sending HTTP requests.
2. Process the response and save the data, save locally.
3. Save the coordinates of all the available banks locally.
4. User can convert one currency to another and switch between the buy and sell values.
5. User can get directions for the closest bank / best option.

## Tools
- *Xcode 7.3 IDE*
- *MapKit*
- *CocoaPods manager*

## Project description

**Application features:**

- Get currency data from all the comercial banks [HTTP / HTML Response]
- Get currency data from the National Bank [HTTPS / XML Response] (disabled)
- Save the currency data locally
- Convert data from one currency to another
- Navigate to the desired bank (only coordinates from a few banks were saved)


**Covered topics:**

1. Swift Language basics
2. Views and View Hierarchy
3. View Controllers
4. Delegation and Text Input
5. UITableView and UITableViewController
6. Subclassing UITableViewCell
7. UINavigationController
8. AutoLayout
9. UIPickerView
10. MapKit
11. Custom Views, custom keyboards
12. UICollectionView
13. Regular Expressions
14. UI Testing (using UI Testing API, XCTest), Unit testing.


####App description

#### Screenshots:
![alt text](AppScreen_01.png "Root view controller displaying the data for all the commercial banks")
![alt text](AppScreen_02.png "Currency Conveter")
![alt text](AppScreen_03.png "Map view")

######Views and elements:
 - ***Root view***
    - **Navigation bar** with a **settings** and **refresh** button. Refresh button is used to check the last data update and reload data if necessary after sending and receiving data from Curs.md or BNM.
    - **UICollectionview**, displays the data.
    -  **Sort button**, on the first click it sorts the data in descending order for Currency-buy, on the next one in descending order for Currency-sell.
    -  **UIPickerView** contains the list of currencies. When a currency is selected, the data for the selected currency will be displayed.
 - *** Currency converter view ***
    - **UITextViews**, top - for buy values, bottom - for sell values.
    - **UIPickerViews**, used to select currencies and configure multipliers.
    - **Custom keyboard view**.
    - **Navigation Bar** with **navigate back button** and **get directions button**.
 - ***Map view ***
    - **MapView**
    - **Navigation Bar** with **navigate back button** and **locate button**
 - *** Settings view***
    - **UITableView**
    - **Navigation Bar** with **navigate back button**


###### Folders, files, classes:
- ***Model***
    - CurrencyData, used for HTTP requests, HTML and XML parsing, also contains data templates.
    - JSONSerializationClass, used for serialization and deserialization
    - ManageFile, used for file operations like create,delete,read and also for retreiving data like file creation date, checking if file exists.
- ***testApplication01***
    - **ViewController**, root view controller.
    - **CurrencyViewController**, currency view controller
    - **MapViewController**, map view controller.
    - **SettingsViewController** (the banksVC will be transformed in settingsVC), settings view controller.
    - **CustomKeyboard** class and xib file. Used for creating a custom keyboard.
    - **Assets**, will contains icons and images.
- ***Pods***
    - imported modules: **Kanna** - used for HTML Parsing, **SGYSwiftJson** - used for data serialization and deserialization.

###### Comments and notes:

The application works on all devices except iphone 4/4s.
Performance of currency converter can be improved by removing the operator buttons and modifying the class responsible for text parsing to something more simpler, without using regex. Purpose of developing this app was to get familiar with ios 9 and swift.

