//
//  Currency_Converter_Tests.swift
//  Currency Converter Tests
//
//  Created by Dorin Gaiu on 4/21/16.
//  Copyright © 2016 Dorin Gaiu. All rights reserved.
//

import XCTest

@testable import Currency_Converter

private extension UIViewController {
  
  func preloadView() {
    let _ = view
  }
}

class Currency_Converter_Tests: XCTestCase {
    
    override func setUp() {
      super.setUp()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
  

    func testViewController() {
      let storyboard = UIStoryboard(name: "Main",
                                    bundle: NSBundle.mainBundle())
      let navigationController = storyboard.instantiateInitialViewController() as! UINavigationController
      let viewController = navigationController.topViewController as! ViewController
      
      XCTAssertNotNil(viewController)
      
      UIApplication.sharedApplication().keyWindow!.rootViewController = viewController
      
      navigationController.preloadView()
      viewController.preloadView()
      
      XCTAssertEqual(viewController.selectedCurrency, "USD")
      
    }
  
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
        }
    }
    
}
