//
//  LocationData.swift
//  testApplication01
//
//  Created by Dorin Gaiu on 4/5/16.
//  Copyright © 2016 Dorin Gaiu. All rights reserved.
//

import UIKit
import MapKit

class LocationData {
  // 47°01'16.6"N 28°50'09.2"E, 47.021276, 28.835898 - maib eminescu
  // 47.017453, 28.843138 - maib stefan 46
  static let listOfLocations: Dictionary<String, CLLocationCoordinate2D> = ["MAIB Eminescu 63": CLLocationCoordinate2D(latitude: 47.021276, longitude: 28.835898),
                                                                     "MAIB Stefan 46": CLLocationCoordinate2D(latitude: 47.017453, longitude: 28.843138),
                                                                     "MAIB Filiala 23, Dacia 27": CLLocationCoordinate2D(latitude: 46.986197, longitude: 28.858772)]
}