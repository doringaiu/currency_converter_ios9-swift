//
//  CustomKeyboard.swift
//  testApplication01
//
//  Created by Dorin Gaiu on 3/16/16.
//  Copyright © 2016 Dorin Gaiu. All rights reserved.
//

import UIKit

private let zeroString = "0"

protocol KeyboardDelegate {
  var sellTextField: UITextView! {get set}
  var buyTextField: UITextView! {get set}
  var multiplier: Double? {get set}
  var invertFlag: Bool {get set}
  func setupMultipliers()
}

class CustomKeyboard: UIView, KBInputDelegate {
  
  @IBOutlet weak private(set) var contentView: UIView!
  var delegate: KeyboardDelegate?
  var kbInput:KBInputParser = KBInputParser()
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    commonInit()
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    commonInit()
  }
  
  func commonInit() {
    loadViewFromNib()
    contentView.frame = bounds
    contentView.backgroundColor = UIColor.clearColor()
    addSubview(contentView)
    kbInput.delegate = self
    
  }
  
  func loadViewFromNib() {
    let nibName: String = String(self.dynamicType)
    let bundle = NSBundle(forClass: self.dynamicType)
    let nib = UINib(nibName: nibName, bundle: bundle)
    nib.instantiateWithOwner(self, options: nil)
  }
  
  func firstResponderChange(sellerIsFirstResponder:Bool, swap: Bool) {
    var tempTextView:UITextView?
    if !sellerIsFirstResponder || sellerIsFirstResponder && swap {
      tempTextView = delegate?.buyTextField
      delegate?.buyTextField = delegate?.sellTextField
      delegate?.sellTextField = tempTextView
      delegate?.sellTextField.text = zeroString
      delegate?.buyTextField.text = zeroString
      delegate?.invertFlag = !(delegate?.invertFlag)!
      delegate?.setupMultipliers()
      resetKBInput()
    }
  }
  
  @IBAction func didTapNumber(sender: UIButton) {
    let senderTitle = sender.currentTitle!
    kbInput.currentValue += senderTitle
    kbInput.parseExpression()
  }
  
  
  @IBAction func didTapOperator(sender: AnyObject) {
    
    let senderTitle = sender.currentTitle!
    if kbInput.expressionState != .OnlyNumber && !kbInput.containsOperator {
      kbInput.currentValue += senderTitle!
      kbInput.parseExpression()
    }
  }
  
  @IBAction func didTapDot(sender: AnyObject) {
    
    let senderTitle = sender.currentTitle!
    if kbInput.expressionState == .AllowAny && !kbInput.containsDot{
      kbInput.currentValue += senderTitle!
      kbInput.containsDot = true
      kbInput.parseExpression()
    }
  }
  
  @IBAction func didTapEqual(sender: AnyObject) {
    // save data
  }
  
  @IBAction func didTapClear(sender: AnyObject) {
    resetKBInput()
  }
  
  func resetKBInput() {
    kbInput.expressionState = .OnlyNumber
    kbInput.currentValue = ""
    delegate?.buyTextField.text = zeroString
    delegate?.sellTextField.text = kbInput.currentValue
    kbInput.containsDot = false
    kbInput.containsOperator = false
  }
  
}



