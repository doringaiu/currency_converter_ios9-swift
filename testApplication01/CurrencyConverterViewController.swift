//
//  CurrencyConverterViewController.swift
//  testApplication01
//
//  Created by Dorin Gaiu on 3/28/16.
//  Copyright © 2016 Dorin Gaiu. All rights reserved.
//

import UIKit
import Google_Material_Design_Icons_Swift

private let zeroString = "0"
private let buyString = "-Buy"
private let sellString = "-Sell"
private let dashString = "-"

protocol CurrencyConverterDelegate {
  var bankData: Dictionary<String, AnyObject> {get set}
}

class CurrencyConverterViewController: UIViewController, KeyboardDelegate, UIPickerViewDelegate, UITextViewDelegate {
  
  
  @IBOutlet weak var navigateBarButton: UIBarButtonItem!
  @IBOutlet weak var backBarButton: UIBarButtonItem!
  @IBOutlet weak var sellTextField: UITextView!
  @IBOutlet weak var buyTextField: UITextView!
  @IBOutlet weak var pickerViewTop: UIPickerView!
  @IBOutlet weak var pickerViewBottom: UIPickerView!
  @IBOutlet weak var imageViewTop: UIImageView!
  @IBOutlet weak var imageViewBottom: UIImageView!
  
  var keyboardView = CustomKeyboard()
  var delegate: CurrencyConverterDelegate?
  var bankData = Dictionary<String, AnyObject>()
  var listOfCurrencies: Array<String> = CurrencyData.listOfCurrencies
  var multipliers: Array<Double?> = [1,1]
  var multiplier: Double?
  var selectedIndex:Int?
  var invertFlag: Bool = false
  private var swapTextViews = false
  private let errorMessage = "Error"
  
  override func viewDidLoad() {
    
    backBarButton.GMDIcon = GMDType.GMDArrowBack
    navigateBarButton.GMDIcon = GMDType.GMDDirectionsWalk
    
    initPickers()
    keyboardView.delegate = self
    sellTextField.inputView = keyboardView
    buyTextField.inputView = keyboardView
    sellTextField.delegate = self
    buyTextField.delegate = self
    setupGradient()
  }
  
  override func viewWillAppear(animated: Bool) {
    sellTextField.becomeFirstResponder()
  }
  
  func setupKeyboardSize() -> CGRect {
    
    switch(DeviceSizes.get_device_type()) {
    case DeviceType.IS_IPHONE_4_OR_LESS:
      return CGRect(x: 0, y: 0, width: 228, height: 228)
    case DeviceType.IS_IPHONE_5:
      return CGRect(x: 0, y: 0, width: 320, height: 320)
    case DeviceType.IS_IPHONE_6:
    return CGRect(x: 0, y: 0, width: 375, height: 375)
    case DeviceType.IS_IPHONE_6P:
      return CGRect(x: 0, y: 0, width: 414, height: 414)
    }
  }
  
  func initPickers() {
    imageViewBottom.image = CurrencyData.listOfFlags[selectedIndex! + 1]
    imageViewTop.image = CurrencyData.listOfFlags[0]
    var currency = listOfCurrencies[selectedIndex!]
    
    currency += buyString
    let coefficient: Double? = Double(self.bankData[currency] as! String)
    if coefficient != nil {
      multipliers[1] = coefficient
      multipliers[0] = 1
      multiplier = 1 / coefficient!
    } else {
      buyTextField.text = "Error"
    }
    listOfCurrencies.insert("MDL", atIndex: 0)
    pickerViewBottom.selectRow(selectedIndex! + 1, inComponent: 0, animated: true)
    keyboardView = CustomKeyboard(frame: setupKeyboardSize())
  }
  
  func setupGradient() {
    
    let gradientLayer = CAGradientLayer()
    let colorLightBlue = UIColor(red: 0, green: 112.0 / 255, blue: 0.8, alpha: 1).CGColor as CGColorRef
    let colorBlue = UIColor(red: 0, green: 122.0 / 255, blue: 1, alpha: 1).CGColor as CGColorRef
    gradientLayer.frame = view.bounds
    gradientLayer.colors = [colorBlue, colorLightBlue]
    gradientLayer.locations = [0.25, 0.75]
    view.layer.insertSublayer(gradientLayer, atIndex: 0)
  }
  
  func textViewDidBeginEditing(textView: UITextView) {
    if buyTextField.isFirstResponder() {
      keyboardView.firstResponderChange(false, swap: swapTextViews)
      swapTextViews = true
      //clearTextViews()
    }
    else if sellTextField.isFirstResponder() && swapTextViews {
      keyboardView.firstResponderChange(true, swap: swapTextViews)
      swapTextViews = false
      //clearTextViews()
    }
  }
  
  func clearTextViews() {
    sellTextField.text = zeroString
    buyTextField.text = zeroString
  }
  
  @IBAction func didTapBack(sender: AnyObject) {
    sellTextField.resignFirstResponder()
    dismissViewControllerAnimated(true, completion: nil)
  }
  
  func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
    return self.listOfCurrencies.count
  }
  
  func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
    return self.listOfCurrencies[row]
  }
  
  func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
    
    setupPickers(row, pickerView: pickerView)
    setupMultipliers()
    keyboardView.resetKBInput()
  }
  
  func setupPickers(row:Int, pickerView: UIPickerView) {
    
    sellTextField.text = zeroString
    buyTextField.text = zeroString
    var currency = self.listOfCurrencies[row]
    
    if buyTextField.text == errorMessage {
      buyTextField.text = zeroString
    }
    
    switch(pickerView){
      
    case pickerViewTop:
      
      imageViewTop.image = CurrencyData.listOfFlags[row]
      
      if currency == dashString {
      }
        
      else if row != 0 {
        currency += buyString
        multipliers[0] = Double(self.bankData[currency] as! String)
      }
        
      else {
        multipliers[0] = 1
      }
      
    case pickerViewBottom:
      
      imageViewBottom.image = CurrencyData.listOfFlags[row]
      if currency == dashString {
      }
        
      else if row != 0 {
        
        currency += sellString
        multipliers[1] = Double(self.bankData[currency] as! String)
      }
      else {
        multipliers[1] = 1
      }
      
    default: break
    }
  }
  
  func setupMultipliers() {
    var isComputable = true
    
    for value in multipliers {
      if value == nil {
          isComputable = false
      }
    }
    
    if isComputable {
      if invertFlag {
          multiplier = multipliers[1]! / multipliers[0]!
        } else {
          multiplier = multipliers[0]! / multipliers[1]!
      }
    } else {
        buyTextField.text = errorMessage
    }
      
    }
  
}
