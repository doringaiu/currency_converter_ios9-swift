//
//  MapViewController.swift
//  testApplication01
//
//  Created by Dorin Gaiu on 3/28/16.
//  Copyright © 2016 Dorin Gaiu. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import Google_Material_Design_Icons_Swift

class MapViewController: UIViewController, CLLocationManagerDelegate {
  
  
  @IBOutlet weak var backBarButton: UIBarButtonItem!
  @IBOutlet weak var locateBarButton: UIBarButtonItem!
  @IBOutlet weak var mapView: MKMapView!
  let locationManager = CLLocationManager()
  
  @IBAction func didTapLocate(sender: AnyObject) {
    locationManager.startUpdatingLocation()
  }
  
  @IBAction func didTapNavigate(sender: AnyObject) {
    dismissViewControllerAnimated(true, completion: nil)
  }
  
  override func viewDidLoad() {
    backBarButton.GMDIcon = GMDType.GMDArrowBack
    locateBarButton.GMDIcon = GMDType.GMDMyLocation
    setupLocations()
    locationManager.delegate = self
    locationManager.desiredAccuracy = kCLLocationAccuracyBest
    locationManager.requestWhenInUseAuthorization()
    locationManager.startUpdatingLocation()
    mapView.showsUserLocation = true
  }
  
  func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    let location = locations.last
    let center = CLLocationCoordinate2D(latitude: location!.coordinate.latitude, longitude: location!.coordinate.longitude)
    let region = MKCoordinateRegion(center: center, span: MKCoordinateSpanMake(0.05, 0.05))
    mapView.setRegion(region, animated: true)
    locationManager.stopUpdatingLocation()
  }
  
  func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
    print(error.localizedDescription)
  }
  
  func setupLocations() {
    var locations = [MKPointAnnotation]()
    for (title,location) in LocationData.listOfLocations {
      let dropPin = MKPointAnnotation()
      dropPin.title = title
      dropPin.coordinate = location
      locations.append(dropPin)
    }
    mapView.addAnnotations(locations)
  }
  
}
