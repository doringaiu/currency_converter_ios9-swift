//
//  CollectionViewCellColumns.swift
//  testApplication01
//
//  Created by Dorin Gaiu on 3/25/16.
//  Copyright © 2016 Dorin Gaiu. All rights reserved.
//

import UIKit

class CollectionViewCellColumns: UICollectionViewCell {
  
  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var buyCurrencyLabel: UILabel!
  @IBOutlet weak var sellCurrencyLabel: UILabel!
  private let darkGrey = UIColor(red: 41.0/255.0, green: 49.0/255.0, blue: 68.0/255.0, alpha: 1)
  private let darkBlue = UIColor(red: 65.0/255.0, green: 72.0/255.0, blue: 90.0/255.0, alpha: 1)

  func setColors(row:Int) {
    
    if row % 2 == 0 {
      backgroundColor = darkGrey
    } else {
      backgroundColor = darkBlue
    }
    
    layer.masksToBounds = true
    layer.borderColor = UIColor.darkGrayColor().CGColor
    layer.borderWidth = 0.5
  }
  
}

