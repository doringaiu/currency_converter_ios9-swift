//
//  ViewController.swift
//  testApplication01
//
//  Created by Dorin Gaiu on 3/14/16.
//  Copyright © 2016 Dorin Gaiu. All rights reserved.
//

import UIKit
import Google_Material_Design_Icons_Swift

private var buyString = "-Buy"
private var sellString = "-Sell"


class ViewController: UIViewController, UIPickerViewDelegate, CurrencyConverterDelegate, UITableViewDelegate, UITableViewDataSource {
  
  @IBOutlet weak var refreshBarButton: UIBarButtonItem!
  @IBOutlet weak var settingsBarButton: UIBarButtonItem!
  @IBOutlet weak var tableViewCurrency: UITableView!
  @IBOutlet weak var sortButton: UIButton!
  
  var data = Array<Dictionary<String, AnyObject>>()
  var bankData = Dictionary<String, AnyObject>()
  var selectedCurrency:String?
  var switchCurrency = true
  var pickerRow:Int = 0
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    refreshBarButton.GMDIcon = GMDType.GMDReplay
    settingsBarButton.GMDIcon = GMDType.GMDSettings
    
    sortButton.layer.borderWidth = 0.5
    sortButton.layer.borderColor = UIColor.darkGrayColor().CGColor
    sortButton.layer.cornerRadius = 20
    sortButton.setGMDIcon(GMDType.GMDSort, forState: .Normal)
    navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
    
    selectedCurrency = "USD" // default setting
    let filemgr = ManageFile()
    
    if filemgr.checkIfExists() {
          data = JSONSerializationClass.deserialize(filemgr.readFile())
    }
  }
  
  override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    if segue.identifier == "CurrencyVC" {
      let destination = segue.destinationViewController as! CurrencyConverterViewController
      destination.delegate = self
      let selectedIndex = tableViewCurrency.indexPathForSelectedRow?.row
      bankData = data[selectedIndex!]
      destination.bankData = bankData
      destination.selectedIndex = pickerRow
    }
  }
  
  func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    return 1
  }
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return data.count
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    
    let cell = tableView.dequeueReusableCellWithIdentifier("firstCell", forIndexPath: indexPath) as! CurrencyRowTableViewCell
    cell.titleLabel.text = data[indexPath.row]["Bank name"] as? String
    let buyCurrency:String? = selectedCurrency! + buyString
    let sellCurrency:String? = selectedCurrency! + sellString
    cell.buyCurrencyLabel?.text = data[indexPath.row][buyCurrency!] as? String
    cell.sellCurrencyLabel?.text = data[indexPath.row][sellCurrency!] as? String
    cell.setColors(indexPath.row)
    cell.selectionStyle = .None
    return cell
  }
  
  override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?){
    view.endEditing(true)
    super.touchesBegan(touches, withEvent: event)
  }
  
  func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
    return 1
  }
  
  func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
    return CurrencyData.listOfCurrencies.count
  }
  
  func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
    return CurrencyData.listOfCurrencies[row]
  }
  
  func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
    self.selectedCurrency = CurrencyData.listOfCurrencies[row]
    self.tableViewCurrency.reloadData()
    pickerRow = row
  }
  
  func pickerView(pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusingView view: UIView?) -> UIView {
    
    var pickerLabel = view as! UILabel!
    
    if view == nil {  //if no label there yet
      pickerLabel = UILabel()
      //color the label's background
      let hue = CGFloat(row)/CGFloat(CurrencyData.listOfCurrencies.count)
      pickerLabel.backgroundColor = UIColor(hue: hue, saturation: 1.0, brightness: 1.0, alpha: 0.05)
      pickerLabel.layer.masksToBounds = true
      pickerLabel.layer.cornerRadius = 10
    }
    
    let titleData = CurrencyData.listOfCurrencies[row]
    let myTitle = NSAttributedString(string: titleData, attributes: [NSFontAttributeName:UIFont(name: "Georgia", size: 30.0)!,NSForegroundColorAttributeName:UIColor.whiteColor()])
    pickerLabel!.attributedText = myTitle
    pickerLabel!.textAlignment = .Center
    return pickerLabel
  }
  
  @IBAction func didTapSort(sender: AnyObject) {
    var currency = selectedCurrency!

    if switchCurrency {
      currency += buyString
    } else {
      currency += sellString
    }
    
    data = CurrencyData.sortByBestValue(data, currency: currency)
    tableViewCurrency.reloadData()
    switchCurrency = !switchCurrency
  }
  
  @IBAction func didTapRefresh(sender: AnyObject) {
    
    let filemgr = ManageFile()
    if filemgr.checkIfExists() {
      let fileElapsedTime = filemgr.getFileElapsedTime()
      let message = String(format: "%.01f",fileElapsedTime) + " Hours Ago"
      let alert = UIAlertController(title: "Last Updated", message: message, preferredStyle: UIAlertControllerStyle.Alert)
      alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: nil))
      alert.addAction(UIAlertAction(title: "Update", style: UIAlertActionStyle.Default, handler: {
        action in
        let curs = CurrencyData()
        if curs.getData() == true {
          self.data = curs.dataCurs
          filemgr.deleteFile()
          filemgr.createFile(JSONSerializationClass.serializer(self.data))
          self.tableViewCurrency.reloadData()
        }
      }))
      self.presentViewController(alert, animated: true, completion: nil)
    } else {
      let curs = CurrencyData()
      
      if curs.getData() == true {
        self.data = curs.dataCurs
        filemgr.createFile(JSONSerializationClass.serializer(self.data))
        self.tableViewCurrency.reloadData()
      }
      
    }
  }
  //viewcontroller
}