//
//  ListOfBanksTableViewController.swift
//  testApplication01
//
//  Created by Dorin Gaiu on 3/21/16.
//  Copyright © 2016 Dorin Gaiu. All rights reserved.
//

import UIKit
import Google_Material_Design_Icons_Swift

class ListOfBanksViewController: UITableViewController {
  
  
  @IBOutlet weak var backBarButton: UIBarButtonItem!
  var options = ["Default Color Scheme", "Red Color Scheme", "Refresh Interval"]
  
  override func viewDidLoad() {
    super.viewDidLoad()
    backBarButton.GMDIcon = GMDType.GMDArrowBack
  }
  
  
  @IBAction func didTapNavigateBack(sender: AnyObject) {
    self.dismissViewControllerAnimated(true, completion: nil)
  }
  
  override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    return 1
  }
  
  override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return options.count
  }
  
  override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! SettingsCustomCellOne
    cell.titleLabel.text = options[indexPath.row]
    cell.currentRow = indexPath.row
    return cell
  }
  
  override func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
  }
  
}
