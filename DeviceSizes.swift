//
//  DeviceSizes.swift
//  testApplication01
//
//  Created by Dorin Gaiu on 4/7/16.
//  Copyright © 2016 Dorin Gaiu. All rights reserved.
//

import UIKit

enum DeviceType {
  case IS_IPHONE_4_OR_LESS
  case IS_IPHONE_5
  case IS_IPHONE_6
  case IS_IPHONE_6P
}

class DeviceSizes {
  
  private struct ScreenSize
  {
    static let SCREEN_WIDTH = UIScreen.mainScreen().bounds.size.width
    static let SCREEN_HEIGHT = UIScreen.mainScreen().bounds.size.height
    static let SCREEN_MAX_LENGTH = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    static let SCREEN_MIN_LENGTH = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
  }
  
  static func get_device_type() -> DeviceType {
    
    if UIDevice.currentDevice().userInterfaceIdiom == .Phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0 {
      return .IS_IPHONE_6
    }
    else if UIDevice.currentDevice().userInterfaceIdiom == .Phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0 {
      return .IS_IPHONE_6P
    }
    else if UIDevice.currentDevice().userInterfaceIdiom == .Phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0 {
      return .IS_IPHONE_5
    } else {
      return .IS_IPHONE_4_OR_LESS
    }
    
  }
  
}

