//
//  Currency_Converter_UITests.swift
//  Currency Converter UITests
//
//  Created by Dorin Gaiu on 4/19/16.
//  Copyright © 2016 Dorin Gaiu. All rights reserved.
//

import XCTest

let emptyString: String = ""

class Currency_Converter_UITests: XCTestCase {
        
  override func setUp() {
    super.setUp()
        
    // Put setup code here. This method is called before the invocation of each test method in the class.
        
    // In UI tests it is usually best to stop immediately when a failure occurs.
    continueAfterFailure = false
    // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
    XCUIApplication().launch()

    // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
  }
    
  override func tearDown() {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    super.tearDown()
  }
  
    
  func testUpdateData() {
    
    let app = XCUIApplication()
    let refreshButton = app.navigationBars["Banks"].buttons.elementBoundByIndex(0)
    refreshButton.tap()
    
    let collectionViewsQuery = app.alerts["Last Updated"].collectionViews
    collectionViewsQuery.buttons["Update"].tap()
    refreshButton.tap()
    collectionViewsQuery.buttons["Cancel"].tap()
    XCTAssert(app.tables.cells.count >= 1)
  }
  
  func testCurrencyVC() {
    
    let app = XCUIApplication()
    app.tables.elementBoundByIndex(0).cells.elementBoundByIndex(0).tap()
    
    let element = app.childrenMatchingType(.Window).elementBoundByIndex(0).childrenMatchingType(.Other).element.childrenMatchingType(.Other).element
    let textViewTop = element.textViews.elementBoundByIndex(0)
    let textViewBottom = element.textViews.elementBoundByIndex(1)
    
    var valueOfRandomButtonPress = tapRandomNumbers(app, taps: 4)
    
    XCTAssertEqual(textViewTop.value as? String, valueOfRandomButtonPress)
    
    textViewBottom.tap()
    
    XCTAssertEqual(textViewBottom.value as? String, emptyString)
    
    valueOfRandomButtonPress = tapRandomNumbers(app, taps: 5)
    
    XCTAssertEqual(textViewBottom.value as? String, valueOfRandomButtonPress)
    
  }
  
  func tapRandomNumbers(app: XCUIApplication, taps: Int) -> String {
    
    var stringResult = emptyString
    
    for _ in 1...taps {
      let index = String(arc4random_uniform(9))
      stringResult += index
      app.buttons[index].tap()
    }
    return stringResult
  }
  
  func testNavigationVC() {
    
  }
  
  
  
  
    
}
