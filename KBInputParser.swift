//
//  KBInputParser.swift
//  testApplication01
//
//  Created by Dorin Gaiu on 4/1/16.
//  Copyright © 2016 Dorin Gaiu. All rights reserved.
//

import UIKit

protocol KBInputDelegate {
  var delegate: KeyboardDelegate? {get set}
}

enum ExpressionStates {
  case OnlyNumber
  case OnlyNumberAndOperator
  case AllowAny
}

extension String {
  subscript (i: Int) -> Character {
    return self[self.startIndex.advancedBy(i)]
  }
}

class KBInputParser {
  var delegate:KBInputDelegate?
  var expressionState: ExpressionStates = .OnlyNumber
  var currentValue:String = ""
  var containsDot:Bool = false
  var containsOperator = false
  private let patternNumberWithoutDot = "(^\\d+)((?!.)(?!\\+)(?!\\-)(?!\\*)(?!\\\\))"
  private let patternUnifinishedNumberWithDot = "(^\\d+\\.$)"
  private let patternNumberWithDot = "(^\\d+\\.\\d+$)"
  private let patternOperator = "([0-9]*\\.[0-9]+|[0-9]+)((\\+$)|(\\-$)|(\\\\$)|(\\*$))"
  private let patternDoesEndWithOperator = "(\\+$)|(\\-$)|(\\\\$)|(\\*$)"
  private let patternCompute = "([0-9]*\\.[0-9]+|[0-9]+)((\\+)|(\\-)|(\\\\)|(\\*))([0-9]*\\.[0-9]+|[0-9]+)"
  
  
  func compute() {
    let multiplier = delegate?.delegate?.multiplier
    
    if rMatch(patternCompute){
      
      let patternOperators = "(\\+)|(\\-)|(\\\\)|(\\*)"
      let location = regexGetPosition(patternOperators)
      let length = currentValue.characters.count - 1
      let numOne = substring(currentValue, start: 0, end: location - 1)
      let numTwo = substring(currentValue, start: location + 1, end: length)
      var result:Double?
      
      switch(currentValue[location]) {
            
        case "+": result = (numOne + numTwo) * multiplier!
        case "-": result = (numOne - numTwo) * multiplier!
        case "*": result = (numOne * numTwo) * multiplier!
        case "\\": result = (numOne / numTwo) * multiplier!
            
        default: break
        }
    
      delegate?.delegate?.buyTextField.text = String(format: "%.2f", result!)
    }
        
    else if rMatch(patternNumberWithoutDot) || rMatch(patternNumberWithDot) {
        
      if !rMatch(patternCompute) {
        let currentNumber = Double(currentValue)
        let result = multiplier! * currentNumber!
        delegate?.delegate?.buyTextField.text = String(format: "%.2f",result)
      }
    }
    
  }
  
  func rMatch(pattern: String) -> Bool {
    let regex = try! NSRegularExpression(pattern: pattern, options: [])
    let match = regex.firstMatchInString(currentValue, options: [], range: NSMakeRange(0, currentValue.characters.count))
    if match != nil {
      return true
    } else {
      return false
    }
  }
  
  func regexGetPosition(pattern: String) -> Int {
    let regex = try! NSRegularExpression(pattern: pattern, options: [])
    let match = regex.firstMatchInString(currentValue, options: [], range: NSMakeRange(0, currentValue.characters.count))
    return (match?.range.location)!
  }
  
  func parseExpression() {
    
    switch(expressionState){
      
    case .OnlyNumber:
        
        if rMatch(patternNumberWithoutDot) {
            containsOperator = false
            expressionState = .AllowAny
        }
            
        else if rMatch(patternNumberWithDot) {
            compute()
            expressionState = .OnlyNumberAndOperator
            containsOperator = false
        }
        
        else if rMatch(patternOperator) {
            expressionState = .AllowAny
            containsOperator = false
        } else {
            expressionState = .AllowAny
            containsOperator = true
        }
        
        compute()
        self.delegate?.delegate?.sellTextField.text = currentValue
        
        
    case .AllowAny:
        
      if rMatch(patternNumberWithoutDot) {
        expressionState = .AllowAny
        compute()
      }
        
      else if rMatch(patternUnifinishedNumberWithDot) {
        expressionState = .OnlyNumber
      } else {
        expressionState = .AllowAny
        containsOperator = true
        compute()
      }
      
      delegate?.delegate?.sellTextField.text = currentValue
      
    case .OnlyNumberAndOperator:
      if rMatch(patternOperator) {
        expressionState = .OnlyNumber
        containsDot = false
        containsOperator = true
      } else {
        expressionState = .OnlyNumberAndOperator
        compute()
      }
      
      delegate?.delegate?.sellTextField.text = currentValue
    }
  }
  
  func substring(str: String, start: Int, end: Int) -> Double {
    let characters = Array(str.characters)
    var result:String = ""
    
    for index in start...end {
      result += String(characters[index])
    }
    
    return Double(result)!
  }
  
}