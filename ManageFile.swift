//
//  FileSave.swift
//  testApplication01
//
//  Created by Dorin Gaiu on 3/25/16.
//  Copyright © 2016 Dorin Gaiu. All rights reserved.
//

import Foundation

class ManageFile {
  var filePath:String?
  
  init() {
    let tempDir = NSTemporaryDirectory()
    filePath = tempDir.stringByAppendingString("data.json")
  }
  
  func checkIfExists() -> Bool {
    let filemgr = NSFileManager.defaultManager()
    if filemgr.fileExistsAtPath(filePath!) {
      return true
    } else {
      return false
    }
  }
  
  private func getFileCreationDate() -> NSDate {
    var attributes:NSDictionary?
    do {
      attributes = try NSFileManager.defaultManager().attributesOfItemAtPath(filePath!)
    } catch {
      print(error)
    }
    return (attributes?.fileCreationDate())!
    
  }
  
  func getFileElapsedTime() -> Double {
    let fileCreationDate = getFileCreationDate()
    let elapsedTime = fileCreationDate.timeIntervalSinceNow
    let hours = Double(Int(elapsedTime)) / -3600
    return hours
  }
  
  func createFile(jsonObject:NSData?) -> Bool{
    let filemgr = NSFileManager.defaultManager()
    if filemgr.fileExistsAtPath(filePath!) {
      return false
    } else {
      filemgr.createFileAtPath(filePath!, contents: jsonObject, attributes: nil)
      return true
    }
  }
  
  func deleteFile() -> Bool{
    let filemgr = NSFileManager.defaultManager()
    do {
      try filemgr.removeItemAtPath(filePath!)
    } catch let error as NSError {
      print(error)
      return false
    }
    return true
  }
  
  func readFile() -> NSData? {
    let dataBuffer: NSData?
    let file: NSFileHandle? = NSFileHandle(forReadingAtPath: filePath!)
    if file == nil {
      print("no such file")
      return nil
    }
    else {
      dataBuffer = file?.readDataToEndOfFile()
      file?.closeFile()
      return dataBuffer
    }
  }
  //eof
}